"""climbing_tracker URL Configuration
"""
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^climbing/', include('climbing.urls')),
    url(r'^admin/', admin.site.urls),
]
