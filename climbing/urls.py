from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'report/(?P<nfc_id>[a-fA-F0-9]+)',
        views.report,
        name='report'),
    url(r'^report-start/$', views.report_start),
    url(r'^report-finish/$', views.report_finish),
]
