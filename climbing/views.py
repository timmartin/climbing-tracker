from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt

import datetime

from .models import Route, Attempt

def index(request):
    nfc_ids = {attempt.nfc_id
               for attempt in Attempt.objects.all()}
    context = {'nfc_ids': nfc_ids}
    return render(request, 'climbing/index.html', context)

def report(request, nfc_id):
    attempts = Attempt.objects.filter(nfc_id=nfc_id)

    week_ago = datetime.datetime.now() - datetime.timedelta(days=7)
    
    recent = attempts.filter(attempt_time__gte=week_ago)
    recent_success = recent.filter(completion_status='SUCCESS')
    
    context = {'recent': recent,
               'percentage_success': len(recent_success) / len(recent) * 100}
    return render(request, 'climbing/report.html', context)

@csrf_exempt
@require_POST
def report_start(request):
    route = get_object_or_404(Route, pk=request.POST['route_id'])

    attempt = Attempt(route=route,
                      nfc_id=request.POST['nfc_id'])
    attempt.save()

    return JsonResponse({'status': 'ok'})


@csrf_exempt
@require_POST
def report_finish(request):
    route = get_object_or_404(Route, pk=request.POST['route_id']);

    attempt = Attempt.objects.filter(nfc_id=request.POST['nfc_id'],
                                     route=route,
                                     completion_status='INCOMPLETE') \
        .order_by('-attempt_time') \
        [0]

    attempt.completion_status = request.POST['completion_status']
    attempt.save()

    return JsonResponse({'status': 'ok'})
