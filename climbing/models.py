from django.db import models

class Route(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Attempt(models.Model):
    attempt_time = models.DateTimeField(auto_now_add=True)

    route = models.ForeignKey('Route', on_delete=models.CASCADE)

    nfc_id = models.CharField(max_length=100)

    COMPLETION_CHOICES = [
        ('INCOMPLETE', 'Attempt not completed'),
        ('SUCCESS', 'Completed successfully'),
        ('FAILURE', 'Completed unsuccessfully')
    ]

    completion_status = models.CharField(max_length=20,
                                         choices=COMPLETION_CHOICES,
                                         default='INCOMPLETE')
