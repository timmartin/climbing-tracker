from django.contrib import admin

from .models import Route, Attempt

admin.site.register(Route)
admin.site.register(Attempt)
